# Cloudron App Migration Tool

This tool is designed to facilitate the migration of Cloudron apps that use the MySQL add-on, using MinIO as the intermediate backup storage. The initial configuration is set up for WordPress but can be adapted for any compatible Cloudron app.

## Important Notes

- This script is designed for Cloudron apps that use the MySQL database add-on, it will not work with different database systems.
- This script needs a healthy, rsync, non encrypted backup.  
- It's recommended to test this script in a staging environment before using it in production.

## Prerequisites

- mc (MinIO Client) installed and configured on your local system.
- cloudron CLI installed and configured on your local system.
- Access to MinIO API with access key and secret key.
- The target Cloudron instance must be accessible and have admin credentials.

## Configuration

You need to set the following variables within the script:

- MINIO_URL: Your MinIO API URL.
- BUCKET_NAME: The name of your bucket in MinIO.
- MINIO_ACCESS_KEY: Your MinIO access key ID.
- MINIO_SECRET_KEY: Your MinIO secret key.
- CLOUDRON_APP_STORE_ID: The store ID of the Cloudron app.

## Usage

Execute the script with command-line arguments:

```bash
./migration_tool.sh -t <target_cloudron_url> -a <target_app_url> -i <source_app_id>
```

Where:
- -t: The URL of the target Cloudron instance.
- -a: The URL where the app will be installed on the target Cloudron.
- -i: The UUID of the source app to migrate.

If the arguments are not provided, the script will prompt you for them interactively.

## Steps Performed by the Script

1. Configure the MinIO Client for communication with your MinIO server.
2. Download the backup from MinIO storage.
3. Zip the downloaded content.
4. Login to the target Cloudron dashboard.
5. Install the app on the target Cloudron.
6. Upload the zipped file to the target Cloudron instance.
7. Extract the content and replace the app data on the target Cloudron.
8. Drop current tables and import the database from the backup.
9. Restart the app on the target Cloudron.


### TODOs

- Support for other type of backup
- Support for Additional Cloudron Addons: Future iterations of the script could include support for additional Cloudron addons. Ideally, the script should be able to detect the required addons directly from the backup's config.json file and handle their migration accordingly.
- In-Place Restore Capability: Implementing a feature to restore the app in place would enhance the script's functionality. Currently, to restore in place, one would need to delete the existing app and ensure that backups are correctly purged during the restore process. Automating this within the script would streamline the migration process.
- Accept config.json as a Parameter: Enhance the script to take a Cloudron backup config.json file as an input parameter. This would allow the script to dynamically configure the migration process based on the settings and data of a specific backup, making the script more versatile and user-friendly.

## Contributing

**PR's are welcome**
Contributions to enhance the script for other types of Cloudron apps or to improve its functionality are welcome. Please fork the repository and submit a pull request with your changes.

