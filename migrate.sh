#!/bin/bash

# Minio related variables
MINIO_URL="https://*your minio api url*"
BUCKET_NAME="your bucket name"
MINIO_ACCESS_KEY="your minio access key id"
MINIO_SECRET_KEY="your minio secret key"

# Wordpress developper cloudron app store id
CLOUDRON_APP_STORE_ID="org.wordpress.unmanaged.cloudronapp"

# Set the fail on pipe error option
set -o pipefail

# Export the variables if they need to be available for sub-processes
export MINIO_URL BUCKET_NAME MINIO_ACCESS_KEY MINIO_SECRET_KEY CLOUDRON_APP_STORE_ID

# Initialize variables
source_app_id=""
target_cloudron_url=""
target_app_url=""

# Function to prompt for user input
prompt_for_input() {
    read -p "Enter source app id (UUID): " source_app_id
    read -p "Enter target Cloudron URL: " target_cloudron_url
    read -p "Enter target app URL: " target_app_url
}

# Parse command-line arguments
while getopts ":t:a:i:" opt; do
  case $opt in
    t) target_cloudron_url=$OPTARG ;;
    a) target_app_url=$OPTARG ;;
    i) source_app_id=$OPTARG ;;
    \?) echo "Invalid option -$OPTARG" >&2; exit 1 ;;
    :) echo "Option -$OPTARG requires an argument." >&2; exit 1 ;;
  esac
done

# If no arguments are provided, prompt for input
if [[ -z $target_cloudron_url ]] || [[ -z $target_app_url ]] || [[ -z $source_app_id ]]; then
    echo "Some arguments were not provided, please enter the missing details:"
    prompt_for_input
fi

dashboard_url="my.${target_cloudron_url}"
folder_name=$(echo $target_app_url | tr '.' '_')

echo "source_app_id: $source_app_id"
echo "target_cloudron_url: $target_cloudron_url"
echo "target_app_url: $target_app_url"

# Function Definitions

configure_mc() {
    mc alias set myminio $MINIO_URL $MINIO_ACCESS_KEY $MINIO_SECRET_KEY --api s3v4
    handle_exit_status $? "MinIO client configured successfully." "Failed to configure MinIO client."
}

download_backup() {
    local backup_path="/$BUCKET_NAME/rsync/snapshot/app_$source_app_id"
    mc cp --recursive "myminio${backup_path}" "$folder_name/"
    handle_exit_status $? "Minio backup downloaded successfully." "Minio backup download failed."
}

zip_content() {
    zip -r "${folder_name}.zip" "$folder_name"
    handle_exit_status $? "Content zipped successfully into ${folder_name}.zip" "Failed to zip the content."
}

login_to_cloudron() {
    cloudron login "$dashboard_url"
    handle_exit_status $? "Logged in to Cloudron successfully." "Failed to log in to Cloudron."
}

install_app() {
    cloudron install --appstore-id "$CLOUDRON_APP_STORE_ID" --location "$target_app_url"
    handle_exit_status $? "App installed successfully on the target Cloudron." "Failed to install the app on the target Cloudron."
}

upload_archive() {
    cloudron push --app "${target_app_url}" "${folder_name}.zip" "/tmp/"
    handle_exit_status $? "Zip archive uploaded successfully to the app's /tmp/ folder." "Failed to upload the zip archive to the app's /tmp/ folder."
}

execute_on_cloudron() {
    cloudron exec --app "${target_app_url}" -- bash -c ' \
      unzip /tmp/"'$folder_name'".zip -d /tmp/ && \
      rm -rf /app/data/* && \
      cp -rT /tmp/"'$folder_name'"/"'app_$source_app_id'"/data/. /app/data/ \
    '
}

import_database() {
    cloudron exec --app "${target_app_url}" -- bash -c ' \
      mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -Nse "show tables" | while read table; do mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "SET FOREIGN_KEY_CHECKS = 0; drop table \`$table\`"; done && \
      mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /tmp/"'$folder_name'"/"'app_$source_app_id'"/mysqldump \
    '
}

restart_app() {
    cloudron restart --app "${target_app_url}"
}

app_already_installed() {
    local app_url=$1
    if cloudron list | grep -q "$app_url"; then
        return 0 # app is already installed
    else
        return 1 # app is not installed
    fi
}

handle_exit_status() {
    if [ $1 -eq 0 ]; then
        echo "$2"
    else
        echo "$3"
        exit 1
    fi
}

# Function to ask user for action when the folder already exists
handle_existing_folder() {
    echo "The folder '$folder_name' already exists."
    read -p "Do you want to delete all and redownload (D) or continue to install the app (C)? [D/C]: " user_choice

    case $user_choice in
        [Dd]* )
            echo "Deleting existing folder and redownloading..."
            rm -rf "$folder_name"
            configure_mc
            download_backup
            zip_content
            ;;
        [Cc]* )
            echo "Continuing with existing data..."
            ;;
        * )
            echo "Invalid input. Please start the script again and choose a valid option."
            exit 1
            ;;
    esac
}

# Main Execution Flow

# Check if the backup folder exists to decide on the action
if [ -d "$folder_name" ]; then
    handle_existing_folder
else
    configure_mc
    download_backup
    zip_content
fi

login_to_cloudron
if ! app_already_installed "$target_app_url"; then
    install_app
else
    echo "The app $target_app_url is already installed on the target Cloudron. Skipping installation."
fi

upload_archive
execute_on_cloudron
import_database
restart_app

echo "Migration completed successfully."
